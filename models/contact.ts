import { Schema, model, Document  } from 'mongoose';
import {ICategory} from './category'
export const contactSchema =  new Schema({
    name: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },
    company_name: {
        type: String,
        required: [true, 'El nombre de la empresa es obligatorio']
    },
    email: {
        type: String,
        required: [true, 'El correo electronico es obligatorio']
    },
    phone: {
        type: Number,
        required: [true, 'El telefono es obligatorio']
    },
    message: {
        type: String,
        required: [true, 'El mensaje es obligatorio']
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    }
})


export interface IContact  extends Document {
    name: String,
    company_name: String,
    email: String,
    phone: Number,
    message: String,
    category: ICategory
}

export const Contact = model<IContact>('Contact', contactSchema);
