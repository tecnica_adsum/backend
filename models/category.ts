import { Schema, model, Document  } from 'mongoose';

export const categorySchema =  new Schema({
    name: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    }
})


export interface ICategory  extends Document {
    name: String,
}

export const Category = model<ICategory>('Category', categorySchema);
