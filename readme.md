## Requirimientos:
- MongoDB v4.4.0
- Nodejs v12.16.3
- NPM v6.14.4

## INSTALAR: 
- Clonar este repositorio
- `npm install typescript -g`
- `npm install`

## Para ejecutar: 
- `tsc`
- `node dist/index`