import { Request, Response } from "express";
import { Contact } from "../models/contact"
import { Category } from '../models/category';

export const create = async(req: Request, res: Response) => {

    try {
        let contact = new Contact(req.body) 

        await contact.save()       

        res.status(201).json({
            ok: true,
            contact
        })
    } catch (error) {

        console.log(error)
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        })
        
    }
};

export const getCategories = async(req: Request, res: Response) => {

    
    const categories = await Category.find()
    
    res.json({
        ok:true,
        categories
    })
}
