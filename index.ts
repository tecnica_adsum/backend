import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import Server from './classes/server';
import {categoriesSeed} from './seeds'
import contactsRoutes from './routes/contac';


const server = new Server();

server.app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});


// Body parser
server.app.use( bodyParser.urlencoded({ extended:true }) );
server.app.use( bodyParser.json() );

// rutas
server.app.use('/contacts', contactsRoutes);

var options = { server: { socketOptions: { keepAlive: 60000, connectTimeoutMS: 30000 } },
                replset: { socketOptions: { keepAlive: 60000, connectTimeoutMS : 30000 } } }

mongoose.connect('mongodb://localhost:27017/tecnica', 
                { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true}, ( error) => {
                    if (error) throw error;

                    console.log('Base de datos online');
                    
                    categoriesSeed()
               });

// levantar express
server.start( () => {
    console.log('Servidor corriendo en puerto 3000');
});



