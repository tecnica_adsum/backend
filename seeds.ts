import {Category} from './models/category'

export const categoriesSeed = () => {
    Category.deleteMany({}, ()=>{})

    Category.create([
        {name: 'uno'},
        {name: 'dos'}
    ])
    
}

