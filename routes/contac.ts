import { Router } from "express";
import { check } from "express-validator";
import { create, getCategories } from "../controllers/contactController";
import { validates } from '../middlewares/validates';

const contactRoutes = Router();

contactRoutes.post('', 
    [
        check('name', 'El nombre es obligatorio' ).not().isEmpty(),
        check('company_name', 'El nombre de la empresa es obligatorio' ).not().isEmpty(),
        check('email', 'El correo electronico es obligatorio o invalido' ).isEmail(),
        check('phone', 'El telefono es obligatorio o invalido' ).isNumeric(),
        check('message', 'El mensaje es obligatorio' ).not().isEmpty(),
        check('category', 'La categoria es boligatoria' ).not().isEmpty(),
        validates
    ],
    create 
);

contactRoutes.get('/categories', [], getCategories)

export default contactRoutes;
